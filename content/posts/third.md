---
title: "Planning in the age of COVID"
date: 2020-05-21T21:50:44-04:00
--- 

My plan was to write something every few days. I want to say "and then the pandemic happened and that all fell to shit." But really, this blog isn't because I have so much to say and a large audience to whom I want interact with. It's to keep me on track and write _something_ and I thought that if I write down some things and publish them it would allow trick my brain into writing other non-blogish types of things -- fiction, poetry, musings, etc. Anything creative really.

But I have ADHD and lot of things I'm interested in for very short amounts of time. I also have a job as a writer and at the end of most days I'm too mentally exhausted to think. I don't want to read or write after I spent the entire day reading and writing. And while I do like my job because the industry is the way it is I'm not scratching that creative itch that got me into writing to begin with. I've yet to really appreciate some aspects of non-fiction writing I suppose but it is what it is.

I've been trying to get things under control. But (and this is where the pandemic comes in) I'm just too out of wack to be productive. I can barely do my job to be honest. My ADHD has been in full force and my depression is just kicking my ass. Normally I'm able to go for a couple of weeks to a month of going along fine before it rears it's head and pushes me into that spiral. But man, it's just been coming every week it seems.

This blog is interesting because I know nobody is reading it so I'm not sure who I am writing to. Am I writing to myself? Or to an imaginary audience? Am I writing to that "one reader" or to the future audience that I'll eventually have when I'm rich and successful. If I'm being honest it's a bit of everything.

Anyway, here's another attempt at trying to make this a regular thing.

- stelees
