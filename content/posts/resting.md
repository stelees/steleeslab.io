+++
title = "restin & testin"
date = 2022-05-14T00:00:00-04:00
draft = false
+++

This is just a small update. I'm enjoying a day off and really just mucking about on my x230. I don't use it as often as I would like. Mainly because the battery is shot and I've been fairly lazy about buying another one. I should put it in my budget but, honestly, paying near $100 for a battery seems absurd. Particularly as I'm using a near decade-old machine.

-- stelees
