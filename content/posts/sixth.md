+++
title = "Time"
date = 2022-01-18T00:00:00-05:00
draft = false
+++

I'm sitting at my kitchen in my underwear. It's about to hit 10 p.m. and I'm listening to a podcast about time. Or, really, I'm listening to On Being with Krista Tippet where the topic is time. It's about time management but really it's about existentialism and time. It's an interesting one to listen to and I encourage everyone who is both intrigued by to-do lists, productivity, but also has a deep distrust in how capitalism shapes our behavior.

I've been using Omnifocus lately. I paid for the pro version as an effort to get my ADHD a little under control and remember all the tasks that I need to do. Lately though I've been noticing that I have a lot of things I **need** to do but very little that I **want** to do. Today I was thinking about how to put in downtime and fun and how to efficiently have a list of things I can do when I'm bored. But I didn't get around to it. Largely because I don't want to put those things down in a productivity app. It feels disingenuous and just a little bit insane when I think of it enough.

On the one hand, I would like to be able to do these things with a certain spontaneity and without planning. But, of course, I have ADHD and that means I'll pass by hours doing nothing and end up hating myself at the end of the day for spending the afternoon binging on a random YouTube channel. I'm not sure what to do here. I mean the answer is obvious to those who are more practical than idealistic. But I find myself switching between the two ideologies fairly frequently.

My ADHD really rebels against the person I try to be and the person I am to **some** extent. I say some because I am my ADHD as much as it is in me. The two are inseparable.

I'm not really terrible at using social media. I can disengage pretty often but my attention span is just all over the place and taking the time to do something that I find interesting, like wood carving, sounds almost impossible. Additionally, I have a few hours in the day after work and my reaction is usually to not do something rather than do something. I'm tired by the end of the day. I don't want to do anything. I want to nap. I **don't** want to make dinner. I **don't** want to wash the dishes. I **don't** want to get work-related things ready for the next day.

The bare basics of living just take so much time. it's frustrating, sure, but it's deeper than that. Some days I just jump from distraction to distraction and none of it are things I **want** to do but, rather, things I want to **feel**. I want to laugh and see interesting things. But why explore what's around me, a city with stores and restaurants I frequent, when I can browse the net and find the vicariously interesting. I can picture my neighborhood in my minds eye, why would I explore that section in the few hours I have? Most of that time will be taken up by traveling from one area that I know to another area that I know.

It's not just the social media distraction. Devices are really just a small part of it all. It seems to me, and I may be wrong this is just a feeling, that we are driven by distraction in nearly all aspects. Is this just me? A hike is a getaway. A brunch with friends is a routine. A walk is a mentally and physically healthy checklist. What does matter? And how do we decide what matters? It is obviously all personal. But what is that deep sense when something switches from trivial to important? That point is something I'm interested in. Device distraction just appears to be a symptom of something larger. It's an aspect sure, but it's not **the** aspect.

Choice is such an interesting word. It's a blessing and a curse. Adams and Eve's dammed for our choices. Every day, every moment we have one choice to make. We make it and then we make another one choice. Only our choices are not our own and they're not our friends or our families. Obligations drive us and we fill the rest with hedonism, driving the boredom away. Viewed through this lense the oroborus switches from an image of the infinite to literally eating ourselves with our choices.

Or maybe it's just me. Maybe I should just try to go to sleep on time for once.

-- stelees
