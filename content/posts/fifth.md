+++
title = "Empty blog update"
date = 2022-01-07T00:00:00-05:00
draft = false
+++

So I moved machines and basically haven't been thinking about this blog. But it's a new year and one of the things I would like to do again is to contribute to this, if only to write more journal-like prose. Something a bit more personal than my usual work. I can't believe it's been two years since I last posted on this. So many things have changed since I last updated this. I have changed of course. But that's to be expected really -- or at least that's the hope.

One of the things I'm struggling with when thinking about this blog is how much of my personal life should I put on this? Or, to put it another way, how much can I say without identifying myself? Privacy is obviously something I'm interested in keeping. But this is a blog after all which is almost like a digital representation of myself and my inner-most thoughts. It's a difficult problem to square. Should I simply talk about my interests? Am I interested in talking about what I'm interested in?

Like most of this short blog I'm mostly thinking about what to write. I want to write. Well, I need to really. It's an impulse and when I don't I feel the absense. Which is a strange compulsion, but it's true. I love to write. I love the expression and the limitless possibilities. From fiction to non-fiction there is just so much to explore. But I'm also pretty indecisive.

Anyways, here's a comic that made me chuckle.

![](/images/cat-comic.jpg)
[kekeflipnote](https://www.instagram.com/p/B4wT46VpVZn/)

-   stelees
