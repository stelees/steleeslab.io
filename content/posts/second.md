---
title: "Endure"
date: 2020-03-08T05:10:04-04:00
---
A common phrase we hear from people talking about those who deal with depression is "bravery." How brave one must be to live with a disease that saps the potential of life. I don't doubt it, but I don't think it quite hits the mark either. Bravery is a choice, or at least it usually is, and the only choice most (i.e. me, since this is obviously a personal blog) choose is to continue living. It's not bravery that keeps them going. It's a remarkable ability to endure. To continue. Some days I know objectively -- as much as one can know -- that it was a good day. That I accomplished good things and that by all normal measures I am loved and cared for. Yet that night I am empty. I don't feel like it was a good day. I feel a hollowness that nothing thus far in my life has been able to fill. And I've tried. 

I am not being brave when -- during those nights which are far too often -- I chose to live another day instead of ending it. I am simply enduring. And often not for myself but for those around me. For those who would have to walk in and find me, and for those who I would hurt by my actions. It's not brave to endure. I feel that's what life is. It's been the constant. Whatever emotions, memories, actions I take, that emptiness will be waiting and I will choose to endure it. The reasons may change but the final choice will always be -- hopefully -- the same.

I hope, of course, that one day I won't have to choose. That I can simply be and feel as though I'm there living alongside myself, instead of waiting and watching for the time when, not matter who is around me, that feeling creeps in. Just a hope, a thought, that helps occasionally.

- steless
