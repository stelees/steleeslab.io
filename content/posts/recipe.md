+++
title = "well that was bland"
date = 2022-06-27T00:00:00-04:00
draft = false
+++

I'm pretty curious how a recipe taken directly from org-chef will look like on this type of blog so this is a little test. But, really, this is also just complaining that the following recipe is super bland.

I suppose it's my fault for continuing with it once I saw that it called for only 2 garlic cloves and one teaspoon on grated ginger. I doubled some of the ingredients like ginger and garlic and soy sauce. I also added some fish and seseme seed oil which made it a little better. Either way wouldn't recommend.

I tried vermicelli noodles from Giant and I'm not sure if there are better brands but it wasn't as great as I'm used to eating either. This is the first time I tried using vermicelli at home so maybe I should have cooked it a bit in the sauce.


## Ginger-Garlic Shrimp With Coconut Milk {#ginger-garlic-shrimp-with-coconut-milk}


### Ingredients {#ingredients}

-   2 large garlic cloves, minced or grated
-   1 teaspoon minced or grated ginger
-   1 teaspoon ground turmeric
-   Kosher salt and black pepper
-   1 tablespoon olive oil
-   1 pound large shrimp, peeled and deveined, tails on or off
-   2 tablespoons vegetable oil
-   1 (14-ounce) can full-fat coconut milk
-   1 tablespoon soy sauce
-   3 packed cups baby spinach
-   1 lime, halved
-   1 fresno, jalapeño or serrano chile, thinly sliced
-   2 scallions, white and light green parts, thinly sliced
-   ½ packed cup cilantro leaves and tender stems, roughly chopped
-   Steamed rice, vermicelli noodles or naan, for serving


### Directions {#directions}

1.  In a mixing bowl, mix together the garlic, ginger, turmeric, 1 teaspoon salt, ½ teaspoon pepper and the olive oil. Add the shrimp and mix to coat well.
2.  Heat the vegetable oil in a large skillet over medium-high until shimmering. Add the shrimp in an even layer and cook, undisturbed, for 2 minutes. Pour in the coconut milk and soy sauce, stir to combine and turn the shrimp. Raise the heat to high and adjust it to maintain a simmer (avoid bringing to a boil), and cook until the liquid is slightly thickened and shrimp are almost cooked through, stirring occasionally, about 3 minutes. Stir in the spinach in batches until wilted.
3.  Remove from the heat and squeeze in the juice from a lime half. Adjust seasoning with more lime and salt as needed. Top with the chiles, scallions and cilantro, and serve with rice, noodles or naan.

-- stelees