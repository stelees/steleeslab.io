+++
title = "ipad"
date = 2022-05-23T00:00:00-04:00
draft = false
+++

(this is a bit of an excited rant)

I wanted a small netbook type computer but I also didn't want to leave the linux/macos system that I've set up.

Largely, aside from the few linux or mac only applications like Onmifocus for mac and the lack of certain games on linux I use the same thing: emacs. Which aside from a few folder changes and the osx additions for emacs on my mac it's the same config file so moving between them is seemless.

I'm certaintly not a programmer but I would be somewhat of a power user. Mainly I work with a lot of text so that's what I've based my workflow around.

While I never had a netbook aside from an one of the first chromebooks -- which is how I first learned how to use linux -- I always wanted something slightly bigger than a phone but not nearly as big as a laptop. I really considered a lot of options but ended up going with the ipad pro with the magic keyboard. Using a tablet is basically the only option to keep my workflow steady and I did not want to go over to Android as I honestly haven't used an android phone is years, I'm already baked into the apple ecosystem, and I wanted to continue to use omnifocus.

Because my own personal use is limited to orgmode I didn't think that using ssh would be that big of a deal. I work in emacs in the terminal all the time when I'm quickly editing a config file or script or just jotting down something in a text file somewhere in emacs. So far the only thing that I haven't been able to do is easily resize the text. I'm not super fucking blind but I'm old enough so one of the first things I always do is make the text bigger. That hasn't worked yet but I'm pretty sure I can figure something out. It's only kinda annoying. I'm writing this on my couch with the ipad basically on my legs stretched out and it's not too hard to see.

I still need to work out all the kinks that may appear and test a few things -- one of which is this blog of course -- and maybe fix up the workflow so it's both smooth and secure, but right now I'm fairly fucking stoked.

I don't like lugging a laptop everywhere as I'm a fairly minimal person. I don't like carrying anything except my keys and phone on most days. But on those days where I'm out and about for longer than a couple of hours I like to bring my bag just in case I decide to post up at a coffee shop or buy something (I try not to use in-store bags as that's just more trash than I want in my life). Even the mac air is just slightly too heavy and large for me to **want** to bring. I'm always weighing the options and I can sometimes be rough with my bag and that shit was/is expensive. Plus I do a lot of work on there and I'm just not comfortable bringing something that has all my documents with me -- even if it is encrypted and backed up.

Anyways, this is really just a test run to see how well it holds up so I'm doing various tasks that I am used to doing. Honestly, this is pretty sweet. The ipad is just big enough that I feel comfortable doing a little bit more complex tasks and small and light enough that I don't mind going somewhere to continue working if I choose.

-- stelees
