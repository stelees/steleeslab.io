+++
title = "microblog"
date = 2022-05-25T00:00:00-04:00
draft = false
+++

I'm kinda wondering if the format I've been using isn't right for what I kinda want to do. I don't think I want to lay out all this work for a blog that no one is reading. And that's not some pessimistic-type approach, I just think it's kinda silly. This really is for me to have a weird creative outlet that is pseudonymous. Honestly I'm rather lazy about the whole thing and I'm just kinda testing the waters but without any real regard for the future.

I suppose ideally I would like this to turn into something, but what that something is I have no idea. The idea of microblogging is interesting but I'm still not really sure what that entails besides a type of twitter like feed. And Twitter is everything I fear about the future of the internet.

Really this has just been rants about my personal life and thoughts on certain aspects of the world. Which is kinda what I want it to be. Right now the blogging space appears largely to be taken up by tech blogs. Which are essentially a long list of small tutorials on whatever that person is working on at the time. While interesting and helpful, is very much not what I like to do going back to the work portion. Additionally, for a lot of those tech blogs there is a slight career-oriented slant to it. Sure, many of them are really just about the hobby but it has the same feel that many budding academics and journalist have when they take to twitter when they enter grad school. Which is mainly just tentative posturing out in the big bad world

This thing is just rants and blabbering. I don't even spell check to be honest or re-read this thing before publishing. Eventually when I actually start to take this a bit seriously and am not simply ranting on whatever I want when I'm bored I'll move it to a proper domain and maybe give it a bit of a redecorating.

For now I'll just take up space on the net.

-- stelees