+++
title = "new blog"
date = 2022-06-27T00:00:00-04:00
draft = false
+++

I'm going to be moving to a new blog soon. While I like this one it was really just testing out different things and workflows. While I do like this style I'm not going to keep this styling either. Not sure if I'll stick to hugo either but I may. I'm still thinking about it.

Ideally the style would be similar to this one -- save maybe in a dark mode -- with some basic tags. I'm not terribly interested in the preview text at the home page either. The new blog will be a bit more personal and with bit more effort going into it. I actually might get into various computer nerdyness this time around.

My job is somewhat around frontier tech and I'm a writer so I think I'll lean into that a bit. It's what interests me and I would like to explore political and social aspects of new technology that I don't often get to at my job.

Still deciding on a domain and handle. I've been online for many many years now and I've never been one to keep a handle for more than a couple of years. But I'm honestly getting a bit tired of all that. I've started and stopped so many blogs in my life that I honestly have lost count. Some of them got a little attention but most, of course, did not.

I hate picking handles. Way too much pressure and I'm certainly not going to use my actual name. I have my own professional website and linkedin for that nonsense. Honestly I'm way too indecisive to pick a new handle anytime soon. Which is frustrating but it is what it is.

I may be a writer by trade but I never liked to connect my professional online presence to the pseudonymous blogs that I enjoy making. There's a certain freedom here that otherwise does not exist. But I would like some stability. I suppose I'm just getting old and want to stick around one place for a little bit.

This handle is really just "let's see" backwards-ish. I've mostly stuck to wordpress and blogger and all that fun stuff so I wanted to see what hugo was like. I enjoy it. It definitely has some limitations though. I might just try to set up my own thing. I have a couple of ideas of what I want.

I might post the new link/domain when I have things set up. But I might not. I don't know yet.

Anyways, those wandering the vast net out there thanks for reading.

-- stelees