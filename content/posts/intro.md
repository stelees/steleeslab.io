---
title: "Intro"
date: 2020-03-08T03:05:16-04:00
---
I've started a blog. It's mostly experimental. I know what I'm not going to explore but I've yet to decide what I am going to focus on. I've chosen this format specifically because I want it to be a freeflowing, semi-anonymous stream dropped into cyberspace. 

- stelees
