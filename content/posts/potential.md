+++
title = "ADHD"
date = 2020-08-17T00:00:00-04:00
draft = false
+++

If I didn't have ADHD, would I be someone else? Would I accomplish all the things I set out to do. Is there another me who exists somewhere in a multi-verse that split off the moment the ADHD "developed" in my prefrontal cortex?

I don't know but I hope not. Wasted potential is a term that terrifies me. I have so much hope and wants but I have to get through this mental illness in order to accomplish anything. The roadblocks are exhausting. I'm exhausted. I would love to just "do" something. To just pursue something when I think of it.

I have a lot of ideas of what to do for this blog but I know many of them aren't going to pan out because I don't have the ability to go through with them. That depresses me so much I can barely stand it. What's the point of being alive if the ideas you have can't actualize because of a disability you have only marginal control over? A rhetorical question mostly. But it's frustrating. To know that there is only so much time in the world and to also know that you are almost destined to waste far too much. I can handle knowing I will waste some. After all relaxation is a must. But knowing that I will waste days and weeks doing nothing while wanting to do something is a ridiculous burden.

-- stelees
