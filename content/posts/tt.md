+++
title = "tiktok"
date = 2022-05-25T00:00:00-04:00
draft = false
+++

Is there anything besides entertainment that is unique to TikTok aside from the fact that it is already a popular app and has shorter videos which TikTok itself contributes toward the shortening attention span?

Do we need more social media app? Are more an intrinsically a net positive for society? Does it seem like it's going in any direction other than letting people create content for other people who create content for other people who create content...

It really does seem like social media is one of the first voluntary capitalist ouroboros. It would be brilliant if it wasn't moving our society in unpredictable and likely dangerous ways.

I'm trying to think of any other product besides social media where the use of the product by other consumers is what creates the value of the product. I can't really think of one but I also just ate an entire bag of frozen fries so I'm not exactly at peak performance right now.

<aside>

this applies to most things in life of course

</aside>

Social media <span class="underline">appears</span> to be the natural progression of communication through technology. Messengers, letters, email, social media. It makes sense. But we confuse the appearence of the natural progression with the idea that the <span class="underline">current</span> variation is <span class="underline">itself</span> inevitible. This is simply not true. Or, if it is, it implies that technology is itself inevitible so why have any laws around the creation of them? (why not just ride some trains and look for john gualt?)

Really, I don't have any moral attributes or arguments. I'd like to think that in pointing out that we are really seeing huge technological shifts and have little idea in which direction it is heading I am stating the extreme obvious. But sometimes there needs to exist a reminder of the utter powerlessness that most (nearly all) of us go about our daily lives.

-- stelees