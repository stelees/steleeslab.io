+++
title = "COVID"
date = 2022-06-13T00:00:00-04:00
draft = false
+++

So after avoiding the damn virus for more than two years it finally caught me. I can't say I'm surprised. It was bound to happen. I came home from a work trip and began to feel a little bit unusual. A bit tired. A bit groggy. My throat scratching just a bit too much for it to be allergies. Ah well. Hopefully it won't be too rough of a ride.

I suppose it really was inevitable. I can't stay in my house all day -- and not just because of work -- and not everybody wears a mask anymore. Honestly it's getting to the point where almost half of the people I see out there don't wear a mask. And this is in more liberal areas.

Well what are you going to do. So far I've just been in bed all day. Catching up on youtube vids and getting the house back in order after being away for a bit.

One thing I'm starting to experiment with is [grocy](https://www.grocy.info) which is an interesting kitchen management app. I haven't really gotten far in it but maybe -- if I'm not feeling too sick -- I can try it out over the next couple of days. Right now I'm just filling out the bare basics. I haven't even gotten to the point where I can add recipes. Plus I'm just putting it in a docker and keeping it local so I don't even know how I can add the grocery list to my phone yet.

Personally, I would like to have some sort of cli kitchen management system but I don't think it exists and I'm no coder. Surprisingly I didn't find anything for Emacs either. There is org-chef but that's really only importing recipes which is just one part of it all.

We'll see if I can actually do anything tomorrow, though.

-- stelees