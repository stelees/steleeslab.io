+++
title = "afk"
date = 2022-05-31T00:00:00-04:00
draft = false
+++

This [Wired](https://www.wired.com/story/bring-back-the-aim-away-message/) article is interesting. The assumed inevitibility -- which is probably spot on -- is alarming because there will only be **more** notifications. Additionally it was apt to point out that the apps themselves all operate independently without a shared protocol so some type of management that isn't individual is unlikely.

Seems like there will eventually be an attempt at coalescing all of the notifications ... but xkdc already spelled that out. Really wonder where the future of notifications will be. I like to assume the worse when it comes to tech so probably won't be anything to feel hopeful about!

-- stelees