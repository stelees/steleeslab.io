+++
title = "Ox-hugo"
date = 2020-08-01
draft = false
+++

I'm testing ox-hugo out because like most emacs users whenever I try something new on the computer I immediately think, "yes but can I do this in emacs?" The answer usually is of course you can. 

It took some fiddling around and I haven't developed a workflow yet. But it basically just exports an org file into a Hugo-readable format. Then I just magit it up into gitlab and wait. Fairly simple and I don't think I need to really dive deep down into workflow here. I would like to set up a capture for some quick blog thoughts but that's about it. This isn't a super complicated blog and I won't worry about tags until this site gets filled a bit.

I use Spacemacs so it was a incredibly easy setup besides telling ox-hugo where to export. And while it recommends to keep everything on one orgfile I like to keep the posts seperate. 

I'm thinking about trying to get a post down every weekend. Right now I'm just meandering around my personal issues but I'll get bored of that eventually and will actually write something interesting -- to me at least. Even though I am using Emacs and writing a blog through Gitlab I won't be exploring coding or programs -- at least not often. I don't code and I'm not really super interested in that field other than how it relates to my own work and I won't be sharing that.

I guess one could say I'm still wondering what this will be about. 

-   steeles
